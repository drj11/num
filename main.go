package main

import (
	"bufio"
	"fmt"
	"os"
)

func main() {
	scanner := bufio.NewScanner(os.Stdin)
	for n := 1; scanner.Scan(); n += 1 {
		fmt.Printf("%d\t%s\n", n, scanner.Bytes())
	}

}
